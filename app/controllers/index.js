var _introController;
var _templateController;

function init() {
	
	//Cargamos la ventana de intro en lo que se carga el home
	_introController = Alloy.createController('intro');
	_introController.window.open();
		
	setTimeout(function(){
		_templateController = Alloy.createController('template', {
			loaded_callback: function(){
				displayTemplate();
		}});
	}, 2000);
} 
init();


function displayTemplate() {

	if (OS_IOS) {
		var navWindow = Ti.UI.iOS.createNavigationWindow({
			window: _templateController.window
		});
	    Alloy.Globals.navigationWindow = navWindow;
	    Alloy.Globals.initNavigation();
	    Alloy.Globals.navigationWindow.open();
	} else {
		Alloy.Globals.initNavigation();
		Alloy.Globals.Navigator.push(_templateController);
	}
	
	//Cerramos la ventana de intro
	_introController.window.close();
	_introController = null;
}