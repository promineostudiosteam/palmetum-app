var Data = require("data");
var ChangeView = require("changeView");

var history_images  = ["history1.png", "history2.png", "history3.png", "history4.png", "history5.png"];
var routeMap_images = ["routeMap1.png", "routeMap2.png", "routeMap3.png", "routeMap4.png", "routeMap5.png"];

var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;

Ti.API.info("Alto del dispositivo -------------> " + Ti.Platform.displayCaps.platformHeight);
Ti.API.info("Ancho del dispositivo ------------> " + Ti.Platform.displayCaps.platformWidth);

var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );


function init(){
	
	//Select a random image to history and routeMap
	var random = Math.round(Math.random() * (history_images.length-1));
	$.home_imageview.image = "/images/home/" + history_images[random];
	  
	random = Math.round(Math.random() * (routeMap_images.length-1));
	$.route_map_imageview.image = "/images/home/" + routeMap_images[random];

	refreshLanguage();
	
	
	//Set Dimensions of elements 
	//Widths
	$.view_home_history.width = $.right_column_view.width 
							  = $.home_imageview.width
							  = $.history_tag.width
							  = $.view_home_routeMap.width
							  = $.route_map_imageview.width
							  = $.route_map_tag.width
							  = $.view_home_parqueMaritimo.width
							  = $.home_parqueMaritimo_imageview.width
							  = $.view_home_parqueMaritimo_tag.width
							  = isTablet ? 300 : 170; 						  
							  
	$.middle_column_view.width = $.view_home_news.width
							   = $.home_news_imageview.width
							   = $.news_tag.width
							   = $.view_home_map.width	
							   = $.home_map_imageview.width	
							   = $.map_tag.width	
				               = isTablet ? 340 : 205;
	
	//ajuste para los dispositivos con resolucion igual o inferior a 480x800			               
	if (Ti.Platform.displayCaps.platformWidth <= 800 && osname === 'android')
	{
		Ti.API.info ("AJUSTANDO EL CONTENIDO");
		
		$.view_home_history.width = $.right_column_view.width 
							  = $.home_imageview.width
							  = $.history_tag.width
							  = $.view_home_routeMap.width
							  = $.route_map_imageview.width
							  = $.route_map_tag.width
							  = $.view_home_parqueMaritimo.width
							  = $.home_parqueMaritimo_imageview.width
							  = $.view_home_parqueMaritimo_tag.width
							  = 155*(Ti.Platform.displayCaps.platformWidth/800);
		$.middle_column_view.width = $.view_home_news.width
							   = $.home_news_imageview.width
							   = $.news_tag.width
							   = $.view_home_map.width	
							   = $.home_map_imageview.width	
							   = $.map_tag.width	
				               = 195*(Ti.Platform.displayCaps.platformWidth/800);					  
		
	}
	
				               
	//Heights
	$.view_home_history.height = $.home_imageview.height
							   = $.middle_column_view.height
							   = $.right_column_view.height
							   = isTablet ? 400 : 228;
							   
						   
							   	   
	$.view_home_news.height = $.home_news_imageview.height = isTablet ? 153 : 87;
	$.view_home_map.height = $.home_map_imageview.height = isTablet ? 240 : 137;
	
	$.view_home_routeMap.height = $.route_map_imageview.height = isTablet ? 283 : 150;
	$.view_home_parqueMaritimo.height = isTablet ? 109 : 74;
	$.home_parqueMaritimo_imageview.height = isTablet ? 75 : 40;
	
}



function refreshLanguage(){
	Data.get_language("home", function(error, e) {
		if (!error) {
			$.label_history_tag.text = e.history_label;
			$.label_news_tag.text = e.news_label;
			$.label_map_tag.text = e.map_label;
			$.label_route_map_tag.text = e.route_map_label;
			$.home_parqueMaritimo_tag.text = e.park_label;
			
		} else {
			Ti.API.info('Error reading config file: ' + error);
		}
	}); 
}

function changeView(e){
	Ti.App.fireEvent(this.id.replace("view_home_", ""));
} 
 

init();