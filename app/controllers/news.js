var RSS = require("rss");
var data = [];
var tableView = null;
var activityIndicator;
var webView = null;

var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;

var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );


function loaded(){
	data = RSS.getData();
} 


Ti.App.addEventListener("dataAvailable", function(){
	loaded();
	loadListView();
});

function init(){
	activityIndicator = Ti.UI.createActivityIndicator({
	  height:Ti.UI.SIZE,
  	  width:Ti.UI.SIZE,
  	  top: "50%",
  	  left: "50%"
	});
	$.content_view.add(activityIndicator);
	activityIndicator.show();
	
	var url = "http://palmetumtenerife.es/noticias/feed/";
	RSS.loadRss(url);
}

function loadListView(){
	if(tableView == null && data.length > 0){
		tableView = Ti.UI.createTableView({ objName: 'tableView' });
		newsSection = Ti.UI.createTableViewSection();
		
		for(var i=0; i < data.length; i++){
			if(i%2 == 0)
				newsSection.add(Ti.UI.createTableViewRow({ title: data[i].title, link: data[i].link, color:"black", height: isTablet ? 60 : 40 , font: isTablet ? {fontSize: 20} : {fontSize: 12}}));
			else	
				newsSection.add(Ti.UI.createTableViewRow({ title: data[i].title, link: data[i].link, color:"black", height: isTablet ? 60 : 40 , font: isTablet ? {fontSize: 20} : {fontSize: 12}, backgroundColor: "#eee"}));
		}
		
		tableView.setSections([newsSection]);	
		
		tableView.addEventListener('click', function(e){
			if (e.source && e.source.objName !== 'tableView'){
				if(e.row.link !== undefined)
					loadNew(e.row.link);
			}
		});
	
	}
	activityIndicator.hide();
	$.content_view.removeAllChildren();
	
	$.content_view.add(tableView);
}

function loadNew(link){
	$.content_view.removeAllChildren();
	activityIndicator = Ti.UI.createActivityIndicator({
	  height:Ti.UI.SIZE,
  	  width:Ti.UI.SIZE
	});
	
	webView = Ti.UI.createWebView({url: link});
	$.content_view.add(webView);
	$.content_view.add(activityIndicator);
	activityIndicator.show();
	
	webView.addEventListener('load', function(){
		activityIndicator.hide();		
	});
}



init();

