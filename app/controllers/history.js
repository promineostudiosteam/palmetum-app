var Data = require("data");
var contentData;
var leftPanel = [];

var currentLabelVisible = null;

var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;
    
	//alert('Titanium.Platform.displayCaps.platformWidth ==>>' + Titanium.Platform.displayCaps.platformWidth);
	//alert('Titanium.Platform.displayCaps.platformHeight ==>>' + Titanium.Platform.displayCaps.platformHeight);
	
	
var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );
var widthLabelAndButtons = isTablet ? (width/3) : 270;

//alert('isTablet =====>' + isTablet);
//alert('screenSizeInch =====>' + screenSizeInch);
	
	//ajuste para los dispositivos con resolucion igual o inferior a 480x800			               
	if (Ti.Platform.displayCaps.platformWidth <= 800 && osname === 'android')
	{
		widthLabelAndButtons = 240;
		
		if (Ti.Platform.displayCaps.platformWidth <= 790)
			{
				widthLabelAndButtons = 170;
			}
	}


var mainLabel = Ti.UI.createLabel({
	width: widthLabelAndButtons,
	visible: false, 
	color: "#444444",
	zIndex: 100
});

function createContentData(){
	$.imageShown_view.visible = false;
	
	//Get content data from JSON file
	Data.get_history(function(error, e) {
		if (!error) {
			contentData = e;
			
		} else {
			Ti.API.info('Error reading content data file: ' + error);
		}
	});
 	
	$.history_label.text = contentData.top.history_label;
	$.top_content_label.text = contentData.top.content;
	
	
	$.left_view.add(mainLabel); 
	
	for(var i=0; i < contentData.main.length; i++){
		var button = Ti.UI.createButton({
			width: widthLabelAndButtons,
			borderRadius: 0,
			borderWidth: 1,
			borderColor: "#00baa4",
			backgroundColor: "#00000000",
			color: "#00baa4",
			textAlign: Titanium.UI.TEXT_ALIGNMENT_LEFT,
			title: contentData.main[i].button,
			top: i*75,
			id: i
		});
		
		button.addEventListener('click', function(e){
			hideLabels();
			if(currentLabelVisible != e.source.id){
				showLabel(e.source.id);
				currentLabelVisible = e.source.id;
			}
			else{
				currentLabelVisible = null;
			}
		});
 		
		leftPanel.push(button);
		
		$.left_view.add(button);
	}
	
	//Set font sizes and colors
	setFonts();
	
	resizeTablet();
}

createContentData();

function hideLabels(){	
	if(mainLabel.getVisible()){
		for(var i=0; i < contentData.main.length; i++){
			if(mainLabel.text == contentData.main[i].content){
				var heightLabel = mainLabel.getSize().height;
				for(var j=i+1; j < contentData.main.length; j++){
					leftPanel[j].top -= heightLabel+20;
				}
				break;
			}
		}
	}
	mainLabel.text = "";
}

function showLabel(label){
	
	mainLabel.text = contentData.main[label].content;
	mainLabel.top = 75 + (label*75);
	
	//Mostramos el mainLabel para obtener su tamaño pero lo ocultamos hasta mover todos los demas elementos
	mainLabel.visible = true;
	mainLabel.opacity = 0;
	setTimeout(function(){
		var heightLabel = mainLabel.getSize().height;
		for(var i=label+1; i < contentData.main.length; i++){	
			leftPanel[i].top += heightLabel+20;
		}
		mainLabel.opacity = 1;
	},250);
}
 
function showImage(){
	$.imageShown.image = this.image;
	$.imageShown_view.visible = true;
}

$.imageShown.addEventListener('click', function(){
	$.imageShown_view.visible = false;
});

function setFonts(){
	//Fonts
	$.history_label.font = isTablet ? {fontSize: 30, fontWeight: 'bold'} : {fontSize: 25, fontWeight: 'bold'};
	$.top_content_label.font = isTablet ? {fontSize: 18} : {fontSize: 15};
	
	//Colors
	$.history_label.color = '#00baa4';
	$.top_content_label.color = '#444444';
}

function resizeTablet(){
	if(osname === 'ipad' || osname === 'iphone'){
		$.content_view.contentHeight = "1500";
	}else{
		$.content_view.contentHeight = "auto";
	}

	if(isTablet){
		$.left_view.width = Ti.Platform.displayCaps.platformWidth / 2;
		$.right_view.width = Ti.Platform.displayCaps.platformWidth / 2;
		
		var rowWidth = Ti.Platform.displayCaps.platformWidth / 2 - 27;
		
		$.first_row_view.width = $.second_row_view.width 
							   = $.third_row_view.width 
							   = $.fourth_row_view.width 
							   = $.fifth_row_view.width 
							   = rowWidth;				   
							   
		var imageWidthAndHeight = rowWidth/3;
		
		
		$.image1.width = $.image1.height
					  = $.image2.width 
					  = $.image2.height
					  = $.image3.width 
					  = $.image3.height
					  = $.image4.width 
					  = $.image4.height
					  = $.image5.width 
					  = $.image5.height
					  = $.image6.width
					  = $.image6.height 
					  = $.image7.width
					  = $.image7.height
					  = $.image8.width 
					  = $.image8.height
					  = $.image9.width 
					  = $.image9.height
					  = $.image10.width 
					  = $.image10.height
					  = $.image11.width 
					  = $.image11.height
					  = $.image12.width 
					  = $.image12.height
					  = $.image13.width 
					  = $.image13.height
					  = $.image14.width 
					  = $.image14.height
					  = $.image15.width 
					  = $.image15.height = imageWidthAndHeight;
	}
	//alert('isTablet ---------> ' + isTablet);
	$.imageShown.width = isTablet ? 650 : 500;
	$.history_label.top = isTablet ? 105 : 0;
	$.top_content_label.top = isTablet ? 135 : 35;
	$.top_view.height = isTablet ? 250 : 200;
	
	
	//alert('$.history_label.top ---------> ' + $.history_label.top);
	//alert('$.top_content_label.top ---------> ' + $.top_content_label.top);
	
	//ajuste para los dispositivos con resolucion igual o inferior a 480x800			               
	if (Ti.Platform.displayCaps.platformWidth <= 800)
	{
		$.imageShown.width = 400*(Ti.Platform.displayCaps.platformWidth/800);
	}
}
