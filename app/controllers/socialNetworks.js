var webView;
var activityIndicator;

var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;

var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );


function loadWebView(e){
	if(this.id == "flickr_view"){
		new Social('https://www.flickr.com/photos/129637884@N04/', 'https://www.flickr.com/photos/129637884@N04/');
	}
	else if(this.id == "youtube_view"){
		new Social('https://www.youtube.com/channel/UComYzDeEyghZf9qHBPr2TGA', 'https://www.youtube.com/channel/UComYzDeEyghZf9qHBPr2TGA');
	}
	else if(this.id == "facebook_view"){
		new Social('fb://page/895443963860847', 'https://www.facebook.com/palmetum.desantacruz'); //FBID --> http://findmyfbid.com/
	}
	else if(this.id == "instagram_view"){
		new Social('https://www.instagram.com/palmetum_tf/', 'https://www.instagram.com/palmetum_tf/'); //instagram://user?username=palmetum_tf
	}
}

function Social(appUrl, webUrl) {
    if (Ti.Android){
        try {
            var intent = Ti.Android.createIntent({
                action: Ti.Android.ACTION_VIEW,
                data: appUrl
            });
            Ti.Android.currentActivity.startActivity(intent);
        } catch (e){
            webView = Ti.UI.createWebView({
            	url : webUrl
            });
            activityIndicator = Ti.UI.createActivityIndicator({
			  height:Ti.UI.SIZE,
			  width:Ti.UI.SIZE
			});
            
            $.content_view.removeAllChildren();
			$.content_view.add(webView);
			
			$.content_view.add(activityIndicator);
			activityIndicator.show();
			
			webView.addEventListener('load', function(){
				activityIndicator.hide();		
			});
        }
    }else{
        if(Titanium.Platform.canOpenURL(appUrl))
        {
            Ti.Platform.openURL(appUrl);
        } else {
        	webView = Ti.UI.createWebView({
            	url : webUrl
            });
            activityIndicator = Ti.UI.createActivityIndicator({
			  top:120,
			  height: 50,
			  width: 50
			});
            
            $.content_view.removeAllChildren();
			$.content_view.add(webView);
			
			$.content_view.add(activityIndicator);
			activityIndicator.show();
			
			webView.addEventListener('load', function(){
				activityIndicator.hide();		
			});                
        }
    }
}

function init(){
	//Resize if Tablet	
	$.facebook_view.width = $.instagram_view.width
						  = $.flickr_view.width
						  = $.youtube_view.width
						  = isTablet ? 700 : 400;
						  
	$.facebook_view.height = $.instagram_view.height
						  = $.flickr_view.height
						  = $.youtube_view.height
						  = isTablet ? 65 : 50;
						 
	$.facebook_view.top = $.youtube_view.bottom
						   = isTablet ? 160 : 0;
						   
	$.instagram_view.top = $.flickr_view.bottom
						    = isTablet ? 270 : 63;
						  
	$.instagram_imageView.top = isTablet ? 20 :8;
						  
}

init();
