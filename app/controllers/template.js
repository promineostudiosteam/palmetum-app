var Data = require("data");
var Sounds = require("sounds");
var ChangeView = require("changeView");
var exit = false;
var exitMessage;
var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;

var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );

var soundsDownloaded = false;
var language;

var activityIndicator;

//Descargamos el JSON
Data.get_map(function(error, e, lang) {
	if (!error) {
		mapData = e;
		language = lang;
		
		Ti.API.info('MAP DATA: ' + JSON.stringify(mapData));
		Sounds.updateSounds(language);
	} else {
		Ti.API.info('Error reading config file: ' + error);
	}
});


var fadeInAnimation = Titanium.UI.createAnimation({
    curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
    opacity: 1,
    duration: 250
});

//Recibimos el evento de descargando para mostrar el feedback
Ti.App.addEventListener('descargandoSonidos', function(e){
	$.downloading_sounds_view.visible = true;
	$.loading_data_view.visible = false;
});

//Cuando tengamos los sonidos listos
Ti.App.addEventListener('descargaSonidosCompletada', function(e){
	//Evita las multiples llamadas
	if(!soundsDownloaded){
		soundsDownloaded = true;
		$.downloading_sounds_view.visible = false;
		$.loading_data_view.visible = false;
		activityIndicator.hide();
	}
});

//Mostramos un error si ha ocurrido algun error
Ti.App.addEventListener('falloAlDescargarSonidos', function(e){
	if(!soundsDownloaded){
		soundsDownloaded = true;
		$.downloading_sounds_view.visible = false;
		$.loading_data_view.visible = false;
		activityIndicator.hide();
	}
});




var refreshLanguage = function (currentView){
	Data.get_language(currentView, function(error, e) {
		if (!error) {
			$.label_current_window.text = e.title;
			
		} else {
			Ti.API.info('Error reading config file: ' + error);
		}
	});
	Data.getExitMessage(function(error, e) {
		if (!error) {
			exitMessage = e.message;
			
		} else {
			Ti.API.info('Error reading config file: ' + error);
		}
	});
};

function changeView(e){
	Ti.App.fireEvent(e.source.id.replace("button_", ""));
	Sounds.stop();
}
function goHome(e){
	Ti.App.fireEvent("home");
	Sounds.stop();
}
function init(){
	$.notifications_view.opacity = 0;
	$.notifications_view.visible = false;
	if(isTablet){
		$.main_content_view.height = height - 80;
	}
	activityIndicator = Ti.UI.createActivityIndicator({
	  height:Ti.UI.SIZE,
  	  width:Ti.UI.SIZE,
  	  top: "70%",
  	  left: "50%",
  	  zIndex: "200"
	});
	$.window.add(activityIndicator);
	activityIndicator.show();
}

init();
resizeTablet();

function resizeTablet(){
	$.button_history.left = $.button_contact.right = isTablet ? 50 : 28 ;
	
	$.button_routeMap.left = $.button_socialNetworks.right = isTablet ? 228 : 123 ;
	
	$.button_news.left = $.button_map.right = isTablet ? 406 : 218 ;
	
	if (Ti.Platform.displayCaps.platformWidth <= 790)
	{
		$.button_history.left = $.button_contact.right = isTablet ? 50 : 28 ;
	
		$.button_routeMap.left = $.button_socialNetworks.right = isTablet ? 228 : 103 ;
	
		$.button_news.left = $.button_map.right = isTablet ? 406 : 188 ;
	}
}

$.close_label.addEventListener('click', function(){
	$.notifications_view.visible = false;
	$.notifications_view.opacity = 0;
});

//Adjust bottom toolbar icon size
$.bottom_toolbar.addEventListener("postlayout", function(){
	$.button_history.width = $.button_history.getRect().height;
	$.button_routeMap.width = $.button_routeMap.getRect().height;
	$.button_news.width = $.button_news.getRect().height;
	$.button_map.width = $.button_map.getRect().height;
	$.button_socialNetworks.width = $.button_socialNetworks.getRect().height;
	$.button_contact.width = $.button_contact.getRect().height;
});
 
$.window.addEventListener('open', function() {
	//Initializes changeView variables
	ChangeView.setMainView($.main_content_view);
	ChangeView.setRefreshLanguage(refreshLanguage);
	
	//Set tittle to Home and load home controller to main view
	refreshLanguage("home");
	_homeController = Alloy.createController('home');
	//_homeController = Alloy.createController('history');
	$.main_content_view.add(_homeController.content_view );
	
	//Hide actionBar Android
    if(Ti.Platform.osname == "android")
    	$.window.activity.actionBar.hide();
    
    $.notification_title_label.font = {fontWeight: 'bold'};
});


 
$.window.addEventListener('android:back',function(e) {
	ChangeView.goBack(function(){
		if (exit == true) {
			//Sounds.stop();
			//$.window.close();
			//Titanium.Android.currentActivity.finish();
		}
		else{
			setTimeout(function(){ exit = false;}, 200);
			
			//var toast = Ti.UI.createNotification({
	   		//	message: exitMessage,
			//    duration: Ti.UI.NOTIFICATION_DURATION_SHORT
			//});
			
			//toast.show();
	    	exit = true;
		}
		
	    return false;
	});
});
 
$.window.open();

// //-----------------------------PUSH NOTIFICATIONS------------------------------- 

// //----------------------------------ANDROID-------------------------------------
if(Ti.Platform.name == "android"){
	
	// Require the modules
	var Cloud = require("ti.cloud");
	var CloudPush = require('ti.cloudpush');
	var deviceToken = null;
	 
	// Initialize the module
	CloudPush.retrieveDeviceToken({
	    success: deviceTokenSuccess,
	    error: deviceTokenError
	});
	// Enable push notifications for this device
	// Save the device token for subsequent API calls
	function deviceTokenSuccess(e) {
	    deviceToken = e.deviceToken;
	    subscribeToChannel();
	}
	function deviceTokenError(e) {
	    alert('Failed to register for push notifications! ' + e.error);
	}
	 
	// Process incoming push notifications
	CloudPush.addEventListener('callback', function (evt) { 
	   	if(Ti.Platform.osname == "android"){
	   		$.notification_title_label.text = JSON.parse(evt.payload).android.title;
	    	$.notification_main_content_label.text = JSON.parse(evt.payload).android.alert;
	   	}
	    
	    $.notifications_view.visible = true;
	    $.notifications_view.animate(fadeInAnimation);
	});
	
	function subscribeToChannel () {
		//Default language english
		var language = "en";
		
		if(Titanium.Locale.currentLanguage == "es")
			language = "es";
		
		    // Specify the push type as either 'android' for Android or 'ios' for iOS
	    Cloud.PushNotifications.subscribeToken({
	        device_token: deviceToken,
	        channel: language,
	        type: Ti.Platform.name == 'android' ? 'android' : 'ios'
	    }, function (e) {
	        if (e.success) {
	            Ti.API.info('Subscribed ----> : ' + language);
	        } else {
	            Ti.API.info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
	        }
	    });
	}

}else{
	// //----------------------------------IOS-------------------------------------
	var deviceToken = null;

	// Wait for user settings to be registered before registering for push notifications
    Ti.App.iOS.addEventListener('usernotificationsettings', function registerForPush() {
        // Remove event listener once registered for push notifications
        Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush); 
 	
        Ti.Network.registerForPushNotifications({
            success: function(e)
			{	
			deviceToken = e.deviceToken;
			deviceTokenSuccessiOS();
			Ti.App.Properties.setString("device_token",deviceToken);
			//alert("Device token received "+deviceToken);
			},
            error: deviceTokenError,
            callback: receivePush
        });
    });
    
    //resgister for push notifications types 
    Titanium.App.iOS.registerUserNotificationSettings({
		types: [
			Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT,
            Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND,
            Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE
	    ],
	});
	
	// Enable push notifications for this device
	// Save the device token for subsequent API calls
	function deviceTokenSuccessiOS(e) {
	    subscribeToChanneliOS();
	}
	function deviceTokenError(e) {
	    alert('Failed to register for push notifications! ' + e.error);
	}
	// Process incoming push notifications
	function receivePush(e) {
	    //alert('Received push: ' + JSON.stringify(e));
	}
	
	// Subscribe with token
	// Require the Cloud module
	var Cloud = require("ti.cloud");
	
	function subscribeToChanneliOS () {
		//default language , english
		var language = 'en';
		
		if(Titanium.Locale.currentLanguage == "es")
		//{
			language = 'es';
		//}
	    // Subscribes the device to the correct channel, depending on the device language
	    // Specify the push type as either 'android' for Android or 'ios' for iOS
	    Cloud.PushNotifications.subscribeToken({
	        device_token: deviceToken,
	        channel: language,
	        type: Ti.Platform.name == 'android' ? 'android' : 'ios'
	    }, function (e) {
	        if (e.success) {
	            //alert('Subscribed in iOS to channel --->' + language);
	            Ti.API.info('Subscribed in iOS to channel ---->' + language);
	        } else {
	            //alert('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
	            Ti.API.info('Error:\n' + ((e.error && e.message) || JSON.stringify(e)));
	        }
	    });
	}
}
