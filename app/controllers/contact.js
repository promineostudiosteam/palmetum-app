var Data = require("data");

var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;

var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );


Data.get_language("contact", function(error, e) {
	if (!error) {
		$.palmetumGarden_label.text = e.palmetumGarden;
		$.contactHere_label.text = e.contactHere;
		$.yourName_label.text = e.name;
		$.subject_label.text = e.subject;
		$.message_label.text = e.message;
		$.sendButton.title = e.sendButton;
	} else {
		Ti.API.info('Error reading config file: ' + error);
	}
}); 


function setFont(){
	$.contactInfo.font = $.yourName_label.font 
					   = $.subject_label.font 
					   = $.message_label.font 
					   = $.yourName_textfield.font 
					   = $.subject_textfield.font 
					   = $.message_textarea.font = isTablet ? {fontSize: 16} : {fontSize: 9};
	
	
	$.palmetumGarden_label.font = $.contactHere_label.font = isTablet ? {fontSize: 30, fontWeight: 'bold'} : {fontSize: 15, fontWeight: 'bold'};
	$.sendButton.font = isTablet ? {fontSize: 25,fontWeight: 'bold'} : {fontWeight: 'bold'};
	
	$.contactInfo.color = $.yourName_label.color 
					   = $.subject_label.color 
					   = $.message_label.color 
					   = $.yourName_textfield.color 
					   = $.subject_textfield.color 
					   = $.message_textarea.color = "black";
}

setFont();

function sendMessage(){
	var emailDialog = Titanium.UI.createEmailDialog();
	emailDialog.subject = $.subject_textfield.value;
	emailDialog.toRecipients = ['info@palmetumsantacruz.com'];
	emailDialog.messageBody = $.yourName_textfield.value + "\n" + $.message_textarea.value;
	emailDialog.open();
}

$.right_panel.addEventListener("click", function(){
	$.yourName_textfield.blur();
	$.subject_textfield.blur();
	$.message_textarea.blur();
});

function blurTextFields(){
	$.message_textarea.blur();
}

function resizeTablet(){
	$.left_panel.width = $.right_panel.width = isTablet ? 412 : 284;
	$.contactHere_label.left = $.palmetumGarden_label.left 
							 = $.scrollView.left 
							 = $.contactInfo.left
							 = isTablet ? 56 : 28;
	$.contactInfo.top = $.scrollView.top = isTablet ? 96 : 48;
	$.contactHere_label.top = $.palmetumGarden_label.top = isTablet ? 34 : 17;
	
	$.scrollView.height = isTablet ? 650 : 180;
	
	$.yourName_textfield.width = $.subject_textfield.width = isTablet ? 260 : 142;
	$.message_textarea.width = isTablet ? 350 : 240; 
	$.sendButton.width = isTablet ? 200 : 115;
	
	$.yourName_textfield.height = $.subject_textfield.height = isTablet ? 50 : 30;
	$.message_textarea.height = isTablet ? 200 : 100; 
	$.sendButton.height = isTablet ? 60 : 30;
	
		
	$.yourName_textfield.top = isTablet ? 30 : 15;
	$.subject_label.top = isTablet ? 90 : 55 ;
	$.subject_textfield.top = isTablet ? 120 : 70 ;  
	$.message_label.top = isTablet ? 180 : 110 ;
	$.message_textarea.top = isTablet ? 210 : 125 ;
	$.sendButton.top = isTablet ? 430 : 235 ;
	
	$.left_panel.left = isTablet ? 45: 0;
	$.right_panel.right = isTablet ? 45: 0;
	
	$.left_panel.top = isTablet ? 40: 0;
	$.right_panel.top = isTablet ? 40: 0;
	
	//Apaño para que se vea bien en dispositivos pequeños , no es lo correcto
	if (Ti.Platform.displayCaps.platformWidth <= 800)
	{
		
		$.left_panel.width = $.right_panel.width = isTablet ? 512 : 284*(Ti.Platform.displayCaps.platformWidth/800);
		$.contactHere_label.left = $.palmetumGarden_label.left 
								 = $.scrollView.left 
								 = $.contactInfo.left
								 = isTablet ? 56 : 28*(Ti.Platform.displayCaps.platformWidth/800);
		$.contactInfo.top = $.scrollView.top = isTablet ? 96 : 48*(Ti.Platform.displayCaps.platformWidth/800);
		$.contactHere_label.top = $.palmetumGarden_label.top = isTablet ? 34 : 17*(Ti.Platform.displayCaps.platformWidth/800);
		
		$.scrollView.height = isTablet ? 650 : 180*(Ti.Platform.displayCaps.platformWidth/800);
		
		$.yourName_textfield.width = $.subject_textfield.width = isTablet ? 284 : 142*(Ti.Platform.displayCaps.platformWidth/800);
		$.message_textarea.width = isTablet ? 400 : 240*(Ti.Platform.displayCaps.platformWidth/800); 
		$.sendButton.width = isTablet ? 200 : 115*(Ti.Platform.displayCaps.platformWidth/800);
		
		$.yourName_textfield.height = $.subject_textfield.height = isTablet ? 50 : 30*(Ti.Platform.displayCaps.platformWidth/800);
		$.message_textarea.height = isTablet ? 200 : 100*(Ti.Platform.displayCaps.platformWidth/800); 
		$.sendButton.height = isTablet ? 60 : 30*(Ti.Platform.displayCaps.platformWidth/800);
		
			
		$.yourName_textfield.top = isTablet ? 30 : 15*(Ti.Platform.displayCaps.platformWidth/800);
		$.subject_label.top = isTablet ? 90 : 55*(Ti.Platform.displayCaps.platformWidth/800) ;
		$.subject_textfield.top = isTablet ? 120 : 70*(Ti.Platform.displayCaps.platformWidth/800) ;  
		$.message_label.top = isTablet ? 180 : 110*(Ti.Platform.displayCaps.platformWidth/800) ;
		$.message_textarea.top = isTablet ? 210 : 125*(Ti.Platform.displayCaps.platformWidth/800) ;
		$.sendButton.top = isTablet ? 430 : 235*(Ti.Platform.displayCaps.platformWidth/800) ;
	}
}

resizeTablet();
