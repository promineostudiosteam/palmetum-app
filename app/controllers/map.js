var Data = require("data");
var Sounds = require("sounds");

var languageData,
	mapData;

var win;	

var mapaLoaded = false;

var interesPoints = [];
var	medicalRoutes = [];
var	gastronomicRoutes = [];
var interesPoints_map = [];
var	medicalRoutes_map = [];
var	gastronomicRoutes_map = [];

var interestEnabled, 
	gastronomicEnabled,
	medicalEnabled;

var currentAudio = null;
var currentPoint = null;
var stopAnimation = true;
var keepPlaying = false;
var elemento = null;

var language;

var osname             	= Ti.Platform.osname,
    height             	= Ti.Platform.displayCaps.platformHeight,
    width             	= Ti.Platform.displayCaps.platformWidth,
    screenWidthInch    	= Titanium.Platform.displayCaps.platformWidth / Titanium.Platform.displayCaps.dpi,
    screenHeightInch	= Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi;

var screenSizeInch      = Math.sqrt( Math.pow(screenWidthInch, 2) + Math.pow(screenHeightInch, 2) );

var isTablet = osname === 'ipad' || (osname === 'android' && screenSizeInch >=6 && (width > 899 || height > 899) );

var baseHeight = isTablet ? 420 : 210;
var baseWidth = isTablet ? 710 : 355;

Ti.API.info("isTablet -----------------> " + isTablet);

//Animations
var fadeIn = Titanium.UI.createAnimation({
    curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT,
    width: 25,
	height: 25,
	borderRadius: 12.5,
    duration: 500
});
var fadeOut = Titanium.UI.createAnimation({
	curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT, 
    width: 20,
	height: 20,
	borderRadius: 10,
    duration: 500
});	
var fadeOutDoubleTap = Titanium.UI.createAnimation({
	curve: Ti.UI.ANIMATION_CURVE_EASE_IN_OUT, 
	opacity: 0,
    duration: 1000
});
fadeIn.addEventListener('complete', function() {
    if(!stopAnimation && currentPoint)
    	currentPoint.animate(fadeOut);
});
fadeOut.addEventListener('complete', function() {
	if(!stopAnimation && currentPoint)
    	currentPoint.animate(fadeIn);
});
fadeOutDoubleTap.addEventListener('complete', function() {
	$.doubleTapToZoom_view.visible = false;
});


function createIosWebView(index){
	
	var htmltext = mapData.points[index].text;
	win = Ti.UI.createWindow();
	win.fullscreen = true;
	
	var webview = Ti.UI.createWebView({
		html: '<font color="white">' + htmltext + '</font>',
		scalesPageToFit: false,
		bottom: '19%',
		top: '19%',
		right: '2%',
		left:'2%'
	});
	webview.backgroundColor = "#00646c";
	win.add(webview);
	win.add($.close_label);
	win.open();
}

function init(){
	//No existe ninguna seccion seleccionada
	interestEnabled = gastronomicEnabled = medicalEnabled = false;
	keepPlaying = false;
	currentAudio = false;
	//Obtenemos los textos de la ventana mapa
	Data.get_language("map", function(error, e) {
		if (!error) {
			languageData = e;
		} else {
			Ti.API.info('Error reading config file: ' + error);
		}
	});
	
	//Descargamos el JSON
	Data.get_map(function(error, e, lang) {
		if (!error) {
			mapData = e;
			language = lang;
			
			Ti.API.info('MAP DATA: ' + JSON.stringify(mapData));
		} else {
			Ti.API.info('Error reading config file: ' + error);
		}
	});
	
	//Set Images to Legend Bar	
	$.map_view.backgroundImage = "/images/map/MapaBase.png";
	$.interes_point_image.image = "/puntosInteres.png";
	$.medical_route_image.image = "/rutaMedicinal.png";
	$.gastronomic_route_image.image = "/rutaGastronomica.png";	
	
	//Set text and size fonts
	setFonts();
} 
 
//Set event listener to map loaded
Ti.App.addEventListener('mapLoaded', function(e){
	setTimeout(function(){
		//Evita las multiples llamadas
		if(!mapaLoaded){
			mapaLoaded = true;
			Sounds.addAudioPlayer($.content_view);
			createAllElements();
		}		
	},1000);
});

//It is called when map.json is loaded and sound are ready
function createAllElements(){
	//Ocultamos la pantalla de carga
	$.loading_data_view.visible = false;
	
	//Mostramos el DoubleTapToZoom y le colocamos un Timeout con un FadeOut
	$.doubleTapToZoom_view.visible = true;
	setTimeout(function(){
		$.doubleTapToZoom_view.animate(fadeOutDoubleTap);
	}, 2000);
	
	createSideBarAndMap();
	
	//Set events to resize MapView and Map_scrollview content 
	$.map_view.addEventListener('doubletap', function(){
		Ti.API.info('DOUBLE TAP ZOOM MAP');
		resizeMapView(); 
	});	
	
	//EVENTS TEXTBOX
	$.close_label.addEventListener('click', function(){
		hideText();
	});
	$.textShown_lightView.addEventListener('click', function(){
		hideText(); 
	});

	//EVENTS AUDIO CONTROLLERS
	$.play_button.addEventListener('click', function(){
		Sounds.resume(null);
		switchPlayButton();
	});
	$.stop_button.addEventListener('click', function(){
		Sounds.stop();
		$.sound_controllers_view.visible = false;	
		currentAudio = null;
		stopAnimation = true;
		
		setTimeout(function(){
			//Stops current Animation
			if(currentPoint != null){
				currentPoint.width = 25;
				currentPoint.height = 25;
				currentPoint.borderRadius = 12.5;
				
				currentPoint = null;
				increaseAllAlphas();
			}
		},600);
		keepPlaying = false;
	});
	
	//GENERAL EVENT TO CONTROLS AUDIO ENDS
	Ti.App.addEventListener('SoundComplete', function(e){
		if(!keepPlaying){
			Sounds.stop();
			$.sound_controllers_view.visible = false;	
			currentAudio = null;
			stopAnimation = true;
			
			setTimeout(function(){
				//Stops current Animation
				if(currentPoint != null){
					currentPoint.width = 25;
					currentPoint.height = 25;
					currentPoint.borderRadius = 12.5;
					
					currentPoint = null;
					increaseAllAlphas();
				}
			},600); 
		}
		else{
			setTimeout(function(){
				keepPlaying = false;
			},1500);
		}	
	});
} 
 
//Establecemos varios textos y tamaños de fuentes
function setFonts(){
	$.interes_point_label.text = languageData.interes_point;
	$.medical_route_label.text = languageData.medical_route;
	$.gastronomic_route_label.text = languageData.gastronomic_route;	
	
	$.interes_point_label.font = $.medical_route_label.font = $.gastronomic_route_label.font = isTablet ? {fontSize:15} : {fontSize:10};
	$.doubleTapToZoom_label.font = {fontSize:20, fontWeight: 'bold'};
}

function createSideBarAndMap(){
	//Texts on the map
	for(var i = 0; i < mapData.texts.length; i++){
		//Posicion del texto con respecto al mapa
		var leftTextMap = (mapData.texts[i].position.x).toString();
		var topTextMap =  (mapData.texts[i].position.y).toString();
		
		var textLabel = Ti.UI.createLabel({
			left: leftTextMap,
			top: topTextMap,
			color: "white",
			text: mapData.texts[i].title,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			font: isTablet ? {fontSize:10, fontWeight: "bold"} : {fontSize:7, fontWeight: "bold"},
			zIndex: 60
		});
		$.map_view.add(textLabel);
	}

	//Points
	for(var i=0; i < mapData.points.length; i++){
		//-----------------Punto i en el SIDEBAR------------------
		//View para un punto en el Sidebar
		var view = Ti.UI.createView({
			width: 213,
			height: isTablet ? 45 : 30,
			left: 0,
			top: isTablet ? i*45 : i*30,
			enabled: true  //Custom property to enable events
		});
		
		
		//Circulo del punto en el Sidebar
		var circle_sidebar = Ti.UI.createLabel({
			width: isTablet ? 40 : 25,
			height: isTablet ? 40: 25,
			left: 0,
			top: 0,
			borderRadius: isTablet ? 20 : 12.5,
           	borderColor: '#88444444',
			color: "white",
			text: mapData.points[i].number,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			font: isTablet ? {fontSize:15, fontWeight: "bold"} : {fontSize:12, fontWeight: "bold"}
		});
		
		//Texto para el punto en el Sidebar
		var textoHtml = null;
		
		if(mapData.points[i].isCientific == "1")
			textoHtml = '<i>' + mapData.points[i].title + '</i>';
		else
			textoHtml = mapData.points[i].title;
		
		
		var title = Ti.UI.createLabel({
			left: isTablet ? 45 : 30,
			top: isTablet ? 12 : 5,
			color: "#00baa4",
			text: mapData.points[i].title,
			html: textoHtml,
			font: isTablet ? {fontSize:15, fontWeight: "bold"} :{fontSize:13, fontWeight: "bold"}
		});
		
		//ajuste para los dispositivos con resolucion igual o inferior a 480x800			               
		if (Ti.Platform.displayCaps.platformWidth <= 800 && osname === 'android' )
		{
			view.width = 180*(Ti.Platform.displayCaps.platformWidth/800);
			view.left = 37;
			circle_sidebar.width = 20*(Ti.Platform.displayCaps.platformWidth/800);
			circle_sidebar.height = 20*(Ti.Platform.displayCaps.platformWidth/800);
			circle_sidebar.font = {fontSize:11, fontWeight: "bold"};
			title.font = {fontSize:12, fontWeight: "bold"};
			
			if (Ti.Platform.displayCaps.platformWidth <= 790)
			{
				view.left = 78;
				circle_sidebar.font = {fontSize:11, fontWeight: "bold"};
				title.font = {fontSize:8.5, fontWeight: "bold"};
			}
		}
		
		//ajuste para tablets con resolucion igual o superior de 800x1200			               
		if (Ti.Platform.displayCaps.platformWidth >= 1200 && osname === 'android' && isTablet == true)
		{
			view.left = 55;
		}
		
		//--------------------------------------------------------
		 
		//-------------------Punto i en el MAPA-------------------
		//Positions
		var leftCircleMap = (mapData.points[i].position.x).toString();
		var topCircleMap =  (mapData.points[i].position.y).toString();
		
		//Circulo del punto en el Mapa
		var circle_map = Ti.UI.createLabel({
			width: 25,
			height: 25,
			left: leftCircleMap,
			top: topCircleMap,
			borderRadius: 12.5,
           	borderColor: '#aaffffff',
			color: "white",
			text: mapData.points[i].number,
			textAlign: Ti.UI.TEXT_ALIGNMENT_CENTER,
			font: {fontSize:12, fontWeight: "bold"}
		}); 
		
		//Set color depends of its kind
		switch(mapData.points[i].type){
			case "blue":
				circle_sidebar.backgroundColor = "#1a6590";
				circle_map.backgroundColor = "#1a6590";
				
				interesPoints.push(view);
				interesPoints_map.push(circle_map);
			break;
			case "green":
				circle_sidebar.backgroundColor = "#28792b";
				circle_map.backgroundColor = "#28792b";
				
				medicalRoutes.push(view);
				medicalRoutes_map.push(circle_map);
			break;
			case "red":
				circle_sidebar.backgroundColor = "#a73735";
				circle_map.backgroundColor = "#a73735";
				
				gastronomicRoutes.push(view);
				gastronomicRoutes_map.push(circle_map);
			break;
			default:
				circle_sidebar.backgroundColor = "#1a6590";
				circle_map.backgroundColor = "#1a6590";
			break;
		}
		//--------------------------------------------------------
		
		//Concatenamos los elementos y los añadimos al Sidebar y al Mapa
		view.add(circle_sidebar);
		view.add(title);
		$.scrollview_slider.add(view);
		$.map_view.add(circle_map);
		//--------------------------------------------------------
		
		
		//Añadimos el evento de click para resaltar un punto
		view.addEventListener('click', function(){
			Ti.API.info('Punto Pulsado' + JSON.stringify(this));
			//Si el punto esta activado
			if(this.enabled){
				//Recorremos los puntos para encontrar el pulsado
				for(var j=0; j < mapData.points.length; j++){
					//Find the clicked button
					if(mapData.points[j].number == this.children[0].text && mapData.points[j].title == this.children[1].text){
						//Enable Audio (if exist) and show TextBox
						//Check if this is not an interest point (Has audio)
						if(this.children[0].text != "·"){
							//If we are currently playing this audio 
							if(this.children[0].text == currentAudio){
								Sounds.resume(function(){
									//Mostramos la ventana con el texto del punto
									//$.textShown.text = mapData.points[j].text;
									if(osname == 'android')
									{
										$.textShown.html = mapData.points[j].text;
										$.hide_text_toast.visible = true;
									}else{
										createIosWebView(j);
										$.close_label.top = 44;
										$.close_label.right = 5;
									}	
									$.textShown_darkView.visible = true;
									
									
									setTimeout(function(){
										$.hide_text_toast.visible = false;
									},3000);
								});
								switchPlayButton();
							}
							//Otherwise
							else{
								//Mostramos la ventana con el texto del punto
								//$.textShown.text = mapData.points[j].text;
								if(osname == 'android')
								{
									$.textShown.html = mapData.points[j].text;
									$.hide_text_toast.visible = true;
								}else{
									createIosWebView(j);
									$.close_label.top = 44;
									$.close_label.right = 5;
								}	
								
								

								$.textShown_darkView.visible = true;
								
								
								setTimeout(function(){
									$.hide_text_toast.visible = false;
								},3000);
								
								//Detenemos el sonido actual
								Sounds.stop();
								keepPlaying = true;

								//Establecemos el nuevo sonido
						        Sounds.setSound(mapData.points[j].number, language);
						        Sounds.play();											//Reproducimos el sonido
						        
						        currentAudio = this.children[0].text;
						        $.sound_controllers_view.visible = true;
						        $.play_button.backgroundImage = "/pause.png";
							}
						}					
						else{ 
							Sounds.stop();
							keepPlaying = true;
							currentAudio = this.children[0].text;			//Cambiamos el currentAudio para desactivarlo
							$.sound_controllers_view.visible = false;
							$.play_button.backgroundImage = "/pause.png";
						} 
						//----------------------------------------------------------

						if(currentPoint != null){
							Ti.API.info('CurrentPoint es != null');
							//Mandamos detener la animacion
							stopAnimation = true;
							//Esperamos a que acabe la actual
							elemento = this.children; 
							setTimeout(function(){
								Ti.API.info('Element: ' + elemento);
								//Devolvemos las medidas normales al punto actual
								currentPoint.width = 25;
								currentPoint.height = 25;
								currentPoint.borderRadius = 12.5;
								
								//Detectamos el punto pulsado
								animateCircleMap(mapData.points[j].type, elemento);
								
								reduceAllAlphas();
								currentPoint.opacity = 1;
							},600);	
						}
						else{
							animateCircleMap(mapData.points[j].type, this.children);
							reduceAllAlphas();
							currentPoint.opacity = 1;
						}
							 
						break;
						
					}
				}
			}
			
		});
		
	}	
}

function disableLegend(e){
	switch(this.id){
		case "interes_point":
			interestEnabled = !interestEnabled;
			
			//Turn off other collections
			if(interestEnabled){
				hideMedicalRoute();
				hideGastronomicRoute();
			}
			//Turn on other collections
			else{
				showMedicalRoute();
				showGastronomicRoute();
			}
			
			//Check if interesPoint are disabled
			if(interesPoints[0].children[0].backgroundColor == "#aaaaaa"){
				showInteresPoint();
			}
			
			medicalEnabled = gastronomicEnabled = false;
			
		break;
		case "medical_route":
			medicalEnabled = !medicalEnabled;
		
			//Turn off other collections
			if(medicalEnabled){
				hideInteresPoint();
				hideGastronomicRoute();
			}
			
			//Turn on other collections
			else{
				showInteresPoint();
				showGastronomicRoute();
			}
			
			//Check if medicalRoutes are disabled
			if(medicalRoutes[0].children[0].backgroundColor == "#aaaaaa"){
				showMedicalRoute();
			}
				
			interestEnabled = gastronomicEnabled = false;
		break;
		case "gastronomic_route":
		
			gastronomicEnabled = !gastronomicEnabled;
		
			//Turn off other collections
			if(gastronomicEnabled){
				hideInteresPoint();
				hideMedicalRoute();
			}
			
			//Turn on other collections
			else{
				showInteresPoint();
				showMedicalRoute();
			}
			
			//Check if gastronomicRoutes are disabled
			if(gastronomicRoutes[0].children[0].backgroundColor == "#aaaaaa"){
				showGastronomicRoute();
			}
		
			interestEnabled = medicalEnabled = false;
		break;
		default:
		break;	
	}
}

function showInteresPoint(){
	for(var i=0; i < interesPoints.length; i++){
		interesPoints[i].children[0].backgroundColor = "#1a6590";
		interesPoints[i].children[1].color = "#00baa4";
		interesPoints[i].enabled = true;
		interesPoints_map[i].visible = true;
	}
}
function showMedicalRoute(){
	for(var i=0; i < medicalRoutes.length; i++){
		medicalRoutes[i].children[0].backgroundColor = "#28792b";
		medicalRoutes[i].children[1].color = "#00baa4";
		medicalRoutes[i].enabled = true;
		medicalRoutes_map[i].visible = true;
	}
}
function showGastronomicRoute(){
	for(var i=0; i < gastronomicRoutes.length; i++){
		gastronomicRoutes[i].children[0].backgroundColor = "#a73735";
		gastronomicRoutes[i].children[1].color = "#00baa4";
		gastronomicRoutes[i].enabled = true;
		gastronomicRoutes_map[i].visible = true;
	}
}
function hideInteresPoint(){
	for(var i=0; i < interesPoints.length; i++){
		interesPoints[i].children[0].backgroundColor = "#aaaaaa";
		interesPoints[i].children[1].color = "#aaaaaa";
		interesPoints[i].enabled = false;
		interesPoints_map[i].visible = false;
	}
}
function hideMedicalRoute(){
	for(var i=0; i < medicalRoutes.length; i++){
		medicalRoutes[i].children[0].backgroundColor = "#aaaaaa";
		medicalRoutes[i].children[1].color = "#aaaaaa";
		medicalRoutes[i].enabled = false;
		medicalRoutes_map[i].visible = false;
	}
}
function hideGastronomicRoute(){
	for(var i=0; i < gastronomicRoutes.length; i++){
		gastronomicRoutes[i].children[0].backgroundColor = "#aaaaaa";
		gastronomicRoutes[i].children[1].color = "#aaaaaa";
		gastronomicRoutes[i].enabled = false;
		gastronomicRoutes_map[i].visible = false;
	}
}

function resizeMapView(){
	if($.map_view.height == baseHeight){
		$.map_scrollview_horizontal.height = $.map_view.height = baseHeight * 2;
		$.map_view.width = baseWidth * 2;
	}
	else{
		$.map_scrollview_horizontal.height = $.map_view.height = baseHeight;
		$.map_view.width = baseWidth;
	}
}

function hideText(){
	$.textShown_darkView.visible = false;
	$.hide_text_toast.visible = false;
	if (osname != 'android'){
	win.hide();
	}
	if(currentAudio != null){
		$.sound_toast.visible = true;
		setTimeout(function(){
			$.sound_toast.visible = false;
		},2000);
	}	
}

function switchPlayButton(){
	$.play_button.backgroundImage = ($.play_button.backgroundImage == "/play.png") ? "/pause.png" : "/play.png";
}

function animateCircleMap(type, element){
	var number = element[0].text;
	var text = element[1].text;
	
	switch(type){
		case "blue":
			for(var i=0; i < interesPoints.length; i++){
				if(interesPoints[i].children[0].text == number && interesPoints[i].children[1].text == text){
					currentPoint = interesPoints_map[i];
					break;
				}
			}
		break;
		case "green":
			for(var i=0; i < medicalRoutes.length; i++){
				if(medicalRoutes[i].children[0].text == number && medicalRoutes[i].children[1].text == text){
					currentPoint = medicalRoutes_map[i];
					break;
				}
			}
		break;
		case "red":
			for(var i=0; i < gastronomicRoutes.length; i++){
				if(gastronomicRoutes[i].children[0].text == number && gastronomicRoutes[i].children[1].text == text){
					currentPoint = gastronomicRoutes_map[i];
					break;
				}
			}
		break;
		default:
		break;
	}
	
	stopAnimation = false;
	currentPoint.animate(fadeOut);
}

function reduceAllAlphas(){
	for(var i=0; i < interesPoints.length; i++){
		interesPoints_map[i].opacity = 0.3;
	}

	for(var i=0; i < medicalRoutes.length; i++){
		medicalRoutes_map[i].opacity = 0.3;
	}

	for(var i=0; i < gastronomicRoutes.length; i++){
		gastronomicRoutes_map[i].opacity = 0.3;
	}
}

function increaseAllAlphas(){
	for(var i=0; i < interesPoints.length; i++){
		interesPoints_map[i].opacity = 1;
	}

	for(var i=0; i < medicalRoutes.length; i++){
		medicalRoutes_map[i].opacity = 1;
	}

	for(var i=0; i < gastronomicRoutes.length; i++){
		gastronomicRoutes_map[i].opacity = 1;
	}
}

Ti.App.addEventListener("closeMapView", function(){
	$.destroy();
});

resizeTablet();
init();

function resizeTablet(){
	//Widths
	$.map_scrollview_vertical.width = $.map_scrollview_horizontal.width 
									= $.sound_controllers_view.width
									= $.doubleTapToZoom_view.width
									= $.loading_data_view.width
									= $.downloading_sounds_view.width
									= $.legend_view.width
									= isTablet ? 710 : 355;
	$.textShown_darkView.width = isTablet ? 1024 : 568;
	$.textShown_lightView.width = isTablet ? 800 : 540;
	
	
	//ajuste para los dispositivos con resolucion igual o inferior a 480x800			               
	if (Ti.Platform.displayCaps.platformWidth <= 800 && osname === 'android' )
	{
			$.textShown_darkView.width = 512*(Ti.Platform.displayCaps.platformWidth/800);
			$.textShown_lightView.width = 486*(Ti.Platform.displayCaps.platformWidth/800);	
	}
	
	$.sidebar_view.width = isTablet ? 270 : 213;
	
	$.interes_point.width = $.medical_route.width
						  = $.gastronomic_route.width
						  = isTablet ? 234 : 117;
						
	//Heights			
	$.map_scrollview_vertical.height = $.map_scrollview_horizontal.height 
									 = $.doubleTapToZoom_view.height
									 = $.loading_data_view.height
									 = $.downloading_sounds_view.height
									 = isTablet ? 420 : 210;
	$.textShown_darkView.height = isTablet ? 715 : 240;	
	$.textShown_lightView.height = isTablet ? 500 : 200;	
	$.sidebar_view.height = isTablet ? 485 : 240;	 					 
								 	  
	$.legend_view.height = $.interes_point.height 
						 = $.medical_route.height
						 = $.gastronomic_route.height
						 = isTablet ? 60 : 30;  
						 
						 
	$.sound_controllers_view.height = isTablet ? 67 : 35;				 

	$.interes_point_image.height = $.interes_point_image.width
								 = $.medical_route_image.height
								 = $.medical_route_image.width
								 = $.gastronomic_route_image.height
								 = $.gastronomic_route_image.width
								 = isTablet ? 35 : 25;
								 
	$.interes_point_label.left   = $.medical_route_label.left
								 = $.gastronomic_route_label.left
								 = isTablet ? 45 : 30;						
								 
	//Paddings
	$.interes_point.left = isTablet ? 10 : 0;				  
	$.medical_route.left = isTablet ? 246 : 118;
	$.gastronomic_route.left = isTablet ? 484 : 237;
	
	$.map_scrollview_vertical.top 	= $.sound_controllers_view.top
									= $.doubleTapToZoom_view.top
									= $.loading_data_view.top
									= $.downloading_sounds_view.top
									= $.sidebar_view.top
									= isTablet ? 100 : 0;
	$.legend_view.bottom = isTablet ? 105 : 0;
	
	$.sound_toast.top = isTablet ? 172 : 40 ;
	$.sound_toast.left = isTablet ? 280 : 102 ;
	
	
	$.hide_text_toast.bottom = isTablet ? 100 : 30 ;
	$.hide_text_toast.left = isTablet ? 437 : 239 ;
	

}
