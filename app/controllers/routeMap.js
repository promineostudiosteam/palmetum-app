// Add in the map module and data for languages
var Map = require('ti.map');
var Data = require("data");
var currentPosition = {latitude: 28.474193, longitude: -16.276724};			//Default user position (it will be overrided)
var nextUpdate = (new Date()).getTime();
var palmetumLocation = {latitude: 28.452106, longitude: -16.256397};
var arriveDistance = 0.003;
var mapview,
	changeModeButton;
var routeMapTexts = {arriveText:"", 
					 hybridMode:"", 
					 normalMode:"", 
					 sateliteMode:"", 
					 resetViewPosition: "", 
					 errorCurrentPosition: "",
					 errorNetworkConnection: "",
					 errorRouteCalculation: ""
					 };

function init(){
	 
	//Loads languages
	Data.get_language("routeMap", function(error, e) {
		if (!error) {
			routeMapTexts.arriveText = e.arriveText;
			routeMapTexts.hybridMode = e.hybridMode;
			routeMapTexts.normalMode = e.normalMode;
			routeMapTexts.sateliteMode = e.sateliteMode;	
			routeMapTexts.resetViewPosition = e.resetViewPosition;	
			routeMapTexts.errorCurrentPosition = e.errorCurrentPosition;
			routeMapTexts.errorNetworkConnection = e.errorNetworkConnection;	
			routeMapTexts.errorRouteCalculation = e.errorRouteCalculation;
		} else {
			Ti.API.info('Error reading config file: ' + error);
		}
	}); 
	
	//Creates mapview
	mapview = Map.createView({
	    userLocation: true,
	    mapType: Map.NORMAL_TYPE,
	    animate: true,
	    userLocationButton: true,
	    userLocation:true,
	    top: 0,
	    left: 0
	});
	
	
	//Add pin to Palmetum Location
	mapview.addAnnotations(
		[Map.createAnnotation({
			latitude: palmetumLocation.latitude,
			longitude: palmetumLocation.longitude,
			title:"Palmetum",
		})]
	);
	
	insertButtonsMapView();
	 
	Ti.Geolocation.getCurrentPosition(function(e){
		if(e.success == true){ 
			currentPosition.latitude = e.coords.latitude;
			currentPosition.longitude = e.coords.longitude; 
			setTimeout(function(){
				mapview.setLocation({
			    	latitude: currentPosition.latitude, longitude: currentPosition.longitude, animate:true, latitudeDelta:0.04, longitudeDelta:0.04
			    });
			    refreshMapRoute(e);
		    }, 500);
		    
		    Ti.API.info('Geolocation: ' + JSON.stringify(e));
	   }
	   else{
	   		alert(routeMapTexts.errorCurrentPosition);
	   }
	}); 
	
	// Refresh when current position changes
	Ti.Geolocation.addEventListener("location", refreshMapRoute); 
	
	$.content_view.add(mapview); 
}

init();


function insertButtonsMapView(){
	changeModeButton = Ti.UI.createButton({
		title: routeMapTexts.normalMode,
		width: 'auto',
		height: 30,
	   	top: 10,
	   	left: 10,
	   	shadowColor: '#50000000',
	   	color: '#dd444444',
	   	backgroundColor: '#bbffffff',
    	shadowOffset: {x:0,y:1},
	});
	
	changeModeButton.addEventListener("click", function(){
		
		if(changeModeButton.title == routeMapTexts.normalMode){
			changeModeButton.title = routeMapTexts.hybridMode;
			mapview.setMapType( Map.HYBRID_TYPE );
		}
		else if(changeModeButton.title == routeMapTexts.hybridMode){
			changeModeButton.title = routeMapTexts.sateliteMode;
			mapview.setMapType( Map.SATELLITE_TYPE );
		}
		else if(changeModeButton.title == routeMapTexts.sateliteMode){
			changeModeButton.title = routeMapTexts.normalMode;
			mapview.setMapType( Map.NORMAL_TYPE );
		}
	});
	
	mapview.add(changeModeButton);
}

//-----------------------------------Rutas------------------------------

//WE USE mapview object TO DISPLAY THE MAP, PLEASE DECLARE IT BEFORE THIS CODE
var route = null;

function refreshMapRoute(e){
	//Refresh every 5 seconds min
	if((new Date()).getTime() > nextUpdate){ 
		nextUpdate = (new Date()).getTime() + 5 * 1000;
		
		if(e.success == true){ 
			currentPosition.longitude = e.coords.longitude;
			currentPosition.latitude = e.coords.latitude;
			
			//Remove old route if exists
			RemoveRoute();
			
			//If have not arrived yet, get new route and set it to mapview
		    if(!arrive()){
		    	GetRoutePoints(currentPosition.latitude, currentPosition.longitude, palmetumLocation.latitude, palmetumLocation.longitude);
		    }		    
	   }
	   else{
	   		Ti.API.info('Error: ' + err);
			alert("Alerta:" + routeMapTexts.errorCurrenPosition);
	   }
	}
}

function decode(str, precision) {
    var index = 0,
        lat = 0,
        lng = 0,
        coordinates = [],
        shift = 0,
        result = 0,
        byte = null,
        latitude_change,
        longitude_change,
        factor = Math.pow(10, precision || 5);

    // Coordinates have variable length when encoded, so just keep
    // track of whether we've hit the end of the string. In each
    // loop iteration, a single coordinate is decoded.
    while (index < str.length) {

        // Reset shift, result, and byte
        byte = null;
        shift = 0;
        result = 0;

        do {
            byte = str.charCodeAt(index++) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);

        latitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

        shift = result = 0;

        do {
            byte = str.charCodeAt(index++) - 63;
            result |= (byte & 0x1f) << shift;
            shift += 5;
        } while (byte >= 0x20);

        longitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

        lat += latitude_change;
        lng += longitude_change;

        coordinates.push([lat / factor, lng / factor]);
    }

    return coordinates;
};

function GetRoutePoints(startX, startY, destX, destY) {
    var url = "http://maps.googleapis.com/maps/api/directions/json?origin="
            + startX + "," + startY + "&destination=" + destX + "," + destY
            + "&sensor=false";
        
    var request = Titanium.Network.createHTTPClient();
    request.onload = function() {
        InitRouteData({
            dashData : [ 1, this.responseText ]
        });
    };
    request.onerror = function() {
        InitRouteData({
            dashData : [ -1, String.serverErr ]
        });
    }; 
    request.open("GET", url);
    request.send(null);
}
 
function InitRouteData(e) {
	if (e.dashData[0] == -1) {
        alert(routeMapTexts.errorNetworkConnection);
    } 
    else{
    	var Json = JSON.parse(e.dashData[1]);
    	
    	//Check if status is OK, if not show an alert
    	if(Json["geocoded_waypoints"][1]["geocoder_status"] == "OK"){
    		var steps = Json["routes"][0]["legs"][0]["steps"]; 
    		var points = [];
    		var polyline;
    		var lengthPopyline;
    		var lengthSteps = steps.length;
    		for ( var i = 0; i < lengthSteps; i++) {
				popyline = decode(steps[i]["polyline"]["points"]);
				lengthPopyline = popyline.length;
				for (var j = 0; j < lengthPopyline; j++){
					points.push({
	            		latitude : popyline[j][0],
	            		longitude : popyline[j][1]
            	 	});
				}
    		}
            route = Map.createRoute({
			    name : "1",
	            points : points,
	            color : "#aaff0000",
	            width : 10
			});
	        mapview.addRoute(route);
    	} 
    	else{
    		alert(routeMapTexts.errorRouteCalculation);
    	}  	
    }
}  
 
function arrive(){
	if(arriveDistance > Math.sqrt(Math.pow((currentPosition.latitude - palmetumLocation.latitude), 2) + Math.pow((currentPosition.longitude - palmetumLocation.longitude),2))){
		alert(routeMapTexts.arriveText);
		Ti.Geolocation.removeEventListener("location",refreshMapRoute);
		return true;
	}
		
	return false;	
}
 
function RemoveRoute(){
    if (route != null) {
        mapview.removeRoute(route);
    }
}
