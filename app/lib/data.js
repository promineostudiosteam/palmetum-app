function loadJsonFile(name, callback, window) {
	//Si estamos cargando los datos del mapa intentamos descargar el fichero mas actualizado
	if(name == "map"){
		var xhr = Titanium.Network.createHTTPClient({
			onload: function() {
				//Creamos el handler para el fichero
				var f = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory,'map.json');
								
				f.write(this.responseData); // Escribimos los datos en el fichero
		
				if (f.exists()) {
					var language = Titanium.Locale.currentLanguage;
					
					var dataSrc = Ti.Platform.osname === 'android' ? ''+f.read() : f.read();
			  		data = JSON.parse(dataSrc);
					
					//Set language to English by default, if system language is not available
					if(data[language] === undefined)
						language = "en";
									
					Ti.App.fireEvent("mapLoaded");
			  		if(window)
			  			callback(null, data[language][window], language);
			  		else
			  			callback(null, data[language], language);
			  		
			  		return; 
				}
			
			 	callback("Error loading JSON file '" + name + "'");
			},
			onerror : function(e) {
				//Cargamos el fichero local si falla la descarga desde internet
				var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory , name + '.json');
		
				if (file.exists()) {
					var language = Titanium.Locale.currentLanguage;
					
					var dataSrc = Ti.Platform.osname === 'android' ? ''+file.read() : file.read();
			  		data = JSON.parse(dataSrc);
					
					//Set language to English by default, if system language is not available
					if(data[language] === undefined)
						language = "en";
					
					Ti.App.fireEvent("mapLoaded");
			  		if(window)
			  			callback(null, data[language][window], language);
			  		else
			  			callback(null, data[language], language);
			  		
			  		return; 
				}
			
			 	callback("Error loading JSON file '" + name + "'");
		     },
			timeout: 10000
		});
		
		xhr.open('GET','http://www.palmetumapp.es/map.json');
		xhr.send();
	}
	else{
		var file = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory , '/data/' + name + '.json');
	
		if (file.exists()) {
			var language = Titanium.Locale.currentLanguage;
			
			var dataSrc = Ti.Platform.osname === 'android' ? ''+file.read() : file.read();
	  		data = JSON.parse(dataSrc);
			
			//Set language to English by default, if system language is not available
			if(data[language] === undefined)
				language = "en";
			
	  		if(window)
	  			callback(null, data[language][window]);
	  		else
	  			callback(null, data[language]);
	  		
	  		return; 
		}
	
	 	callback("Error loading JSON file '" + name + "'");
	}
} 

/**
 * Data 
 */
var Data = {
	get_language: function(window, callback) {
		loadJsonFile('language', callback, window);
	},
	
	getExitMessage: function(callback){
		loadJsonFile('language', callback, "exitMessage");
	},
	
	get_history: function(callback){
		loadJsonFile('history', callback);
	},
	
	get_map: function(callback){
		loadJsonFile('map', callback);
	}
	
};

module.exports = Data;