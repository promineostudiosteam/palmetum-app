var data;
var Rss = {
	loadRss: function (url) {
		loadUrl(url, function onLoad(err, xml) {

			if (err) {
				var toast = Ti.UI.createNotification({
		   			message: "Error loading RSS feeds",
				    duration: Ti.UI.NOTIFICATION_DURATION_SHORT
				});
				toast.show();
				Ti.API.info('Error loading Feeds: ' + err);
				return;
			}

			try {				
				data = parseXML(xml);
				
				Ti.App.fireEvent("dataAvailable");
				
			} catch (e) {
				var toast = Ti.UI.createNotification({
		   			message: "Error loading data",
				    duration: Ti.UI.NOTIFICATION_DURATION_SHORT
				});
				toast.show();
				Ti.API.info('Error loading data: ' + err);
				return;
			}

		});
	},
	getData: function(){
		return data;
	}
};

module.exports = Rss;

function parseXML(xml) {
	var models = [];

	var elements = xml.documentElement.getElementsByTagName('item');
	var element, model, childNodes, child, i, j;

	// for each <item>
	for (i = 0; i < elements.length; i++) {
		element = elements.item(i);
		model = {};

		childNodes = element.childNodes;

		// for all childNodes
		for (j = 0; j < childNodes.length; j++) {
			child = childNodes.item(j);

			// if the child is an element containing a text or CDATA node
			if (child.nodeType === child.ELEMENT_NODE && child.childNodes.length === 1 && [child.TEXT_NODE, child.CDATA_SECTION_NODE].indexOf(child.childNodes.item(0).nodeType) !== -1) {
				// set or append if model already has a property with the same name
				model[child.nodeName] = model[child.nodeName] ? (_.isArray(model[child.nodeName]) ? model[child.nodeName] : [model[child.nodeName]]).concat(child.textContent) : child.textContent;
			}
		}

		models.push(model);
	}

	return models;
}

function loadUrl(url, callback) {
	// fetch the URL
	var xhr = Titanium.Network.createHTTPClient({
		onload: function onload(e) {
			var xml = this.responseXML;

			// response was no XML
			if (xml === null || xml.documentElement === null) {
				return callback(String.format('Response did not contain XML: %s', url));
			}

			callback(null, xml);
		},

		onerror: function onerror(e) {
			callback(String.format('Request failed: ' + e.error));
		}
	});

	xhr.open('GET', url);
	xhr.send();
}
