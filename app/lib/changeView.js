/**
 * Change View Module 
 */
var mainView;
var _currentView = "";
var _currentController;
var refreshLanguage;
var history = ["home"];

var ChangeView = {
	setMainView: function(view){
		mainView = view;
	},
	setRefreshLanguage: function(refresh){
		refreshLanguage = refresh;
	},
	goBack: function(callback){
		Ti.API.info('History: ' + JSON.stringify(history));
		if(history.length == 0){
			callback();
		}
		else{
			//elimino la ultima entrada para que no se repita 
			history.pop();
			//cambio la vista a la anterior si la hay
			if (history[history.length-1] != null)
			changeView(history[history.length-1], false);
			
			//he borrado todo el historial y establezco de nuevo el home como el primero
			if (history.length == 0){
				history.push("home");
			}
		}
	}
};

function changeView(e, addHistory){
	
	if(addHistory == true && history.length >= 1 && history[history.length-1] != e){
		history.push(e);
	}
	
	if(e != "map"){
		Ti.App.fireEvent("closeMapView");
	}
	switch(e){
		case "home":
			_currentView = "home";
			_currentController = Alloy.createController('home');
			refreshLanguage("home");
			break;
		case "history":
			_currentView = "history";
			_currentController = Alloy.createController('history');
			refreshLanguage("history");
			break;
			
		case "routeMap":
			if(_currentView != "routeMap"){
				_currentController = Alloy.createController('routeMap');
				refreshLanguage("routeMap");
				_currentView = "routeMap";
			}
			else return;
			break;
			
		case "news":
			_currentView = "news";
			_currentController = Alloy.createController('news');
			refreshLanguage("news");
			break;
			
		case "map":
			if(_currentView != "map"){
				_currentController = Alloy.createController('map');
				refreshLanguage("map"); 
				_currentView = "map";
			}
			else return;
			break;
		
		case "socialNetworks":
			_currentView = "socialNetworks";
			_currentController = Alloy.createController('socialNetworks');
			refreshLanguage("socialNetworks");
			break;
			
		case "contact":
			if(_currentView != "contact"){
				_currentController = Alloy.createController('contact');	
				refreshLanguage("contact");
				_currentView = "contact";
			} 
			else return; 
			break; 
		case "parqueMaritimo":
			_currentView = "parqueMaritimo";
			_currentController = Alloy.createController('parqueMaritimo');
			refreshLanguage("parqueMaritimo");
			break; 
		default:
			  
			break; 
	}
	
	mainView.removeAllChildren();
	mainView.add(_currentController.content_view);
}

Ti.App.addEventListener('home', function(e){
	changeView('home',true);
});
Ti.App.addEventListener('history', function(e){
	changeView('history',true);
});
Ti.App.addEventListener('routeMap', function(e){
	changeView('routeMap',true);
});
Ti.App.addEventListener('news', function(e){
	changeView('news',true);
});
Ti.App.addEventListener('map', function(e){
	changeView('map',true);
});
Ti.App.addEventListener('socialNetworks', function(e){
	changeView('socialNetworks',true);
});	
Ti.App.addEventListener('contact', function(e){
	changeView('contact',true);
});
Ti.App.addEventListener('parqueMaritimo', function(e){
	changeView('parqueMaritimo',true);
});


module.exports = ChangeView;