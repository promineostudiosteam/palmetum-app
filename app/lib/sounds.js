var urlSound = null;
var audioPlayer = null;

if(Ti.Platform.name == "android"){
	audioPlayer = Titanium.Media.createVideoPlayer({
	    mediaTypes: Titanium.Media.VIDEO_MEDIA_TYPE_AUDIO,
	    autoplay: false,
	    visible: false
	});
}else{
	audioPlayer = Titanium.Media.createAudioPlayer({
	    autoplay: false
	});
}


var Sounds = {
	play: function(){
		audioPlayer.play();
	},
	resume: function(callback) {
		//Si estamos en Android
		if (Ti.Platform.name == "android"){
			//Si esta pausado, lo continuamos
			if (audioPlayer.playbackState == Ti.Media.VIDEO_PLAYBACK_STATE_PAUSED){
				audioPlayer.play();
		        if(callback != null)
		        	callback();
			}
			//Si se esta reproduciendo
		    else {
		        audioPlayer.pause();
		    }
		}
		//Si estamos en iOS
		else{
			if (audioPlayer.paused) {
		        audioPlayer.play();
		        if(callback != null)
		        	callback();
		    }
		    //Si se esta reproduciendo
		    else {
		        audioPlayer.pause();
		    }
		}
	
	},
	stop: function(){
		audioPlayer.stop();
		if(Ti.Platform.name == "android"){
        	audioPlayer.release();
        }
	},
	setSound: function(numberFile, language){
		var f;
		if(Ti.Platform.name == "android"){
			if(Ti.Filesystem.isExternalStoragePresent()){
				f = Ti.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory,  language + "/" + numberFile + ".mp3");
				audioPlayer.setUrl(f.nativePath);
			}
		}
		else{
			f = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,  language + "/" + numberFile + ".mp3");
			audioPlayer.setUrl(f.nativePath);
		}
		
	},
	addAudioPlayer: function(view){
		//Colocamos el audioPlayer en la view
		view.add(audioPlayer);
	},
	updateSounds: function(language){
		Ti.API.info('UPDATE SOUNDS' + language);
		var url = "http://www.palmetumapp.es/audiosMapa/" + language + "/version.txt";
		
		//Si estamos en android y tenemos SD Card o estamos en iOS
		if(Ti.Platform.name != "android" || Ti.Filesystem.isExternalStoragePresent()){
			var xhr = Titanium.Network.createHTTPClient({
				onload: function() {				
					//Comprobamos el fichero de version para comprobar si existen actualizaciones, y si es asi, descargamos los sonidos
					checkVersionFile(this.responseData, language);
				},
				onerror : function(e) {
					Ti.API.info('ERROR AL DESCARGAR EL FICHERO VERSION');
					Ti.App.fireEvent("descargaSonidosCompletada");
			    },
				timeout: 10000
			});	
			xhr.open('GET', url);
			xhr.send();
		}
	}
};

function checkVersionFile(newFileContent, language){
	var ficheroNuevo, ficheroLocal;		//Referencias a los ficheros de versiones
	var assetsdir;						//Directorio para los sonidos
	var newassetsdir;					//Handler para el directorio de sonidos
	var numberOfFilesToDownload;		//Numero de ficheros a descargar
	var nombreFicheroLocal = "version"+ language + ".txt";
	
	//Creamos los handlers para los dos ficheros dependiento de la plataforma asi como los directorios
	if(Ti.Platform.name == "android"){
		ficheroNuevo = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, 'version.txt');				
		ficheroLocal = Ti.Filesystem.getFile(Ti.Filesystem.externalStorageDirectory, nombreFicheroLocal);
		assetsdir = Titanium.Filesystem.externalStorageDirectory + language + "/";
	}
	else{
		ficheroNuevo = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, 'version.txt');				
		ficheroLocal = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, nombreFicheroLocal);
		assetsdir = Titanium.Filesystem.applicationDataDirectory + language + "/";
	}
	
	ficheroNuevo.write(newFileContent); // Escribimos los datos descargados en el fichero nuevo
	
	//ficheroLocal.deleteFile();
	
	newassetsdir = Titanium.Filesystem.getFile(assetsdir);		//Creamos el directorio si no existe
	if (!newassetsdir.exists()) {
	    newassetsdir.createDirectory();
	}
	
	//Si no tenemos un fichero de version Local o existe una actualizacion, descargamos los sonidos
	if (!ficheroLocal.exists() || ficheroNuevo.read().text != ficheroLocal.read().text) {					 
		//Actualizamos el fichero local con la nueva version
		ficheroLocal.write(ficheroNuevo.read());
		
		//Avisamos que vamos a descargar sonidos
		Ti.App.fireEvent("descargandoSonidos");
		
		Ti.API.info('DescargandoSonidos....');
		numberOfFilesToDownload = Number(ficheroNuevo.read().text);

		Ti.API.info('Numero de Sonidos a Descargar: ' + numberOfFilesToDownload);
		//Descargamos los sonidos
		var baseUrl = "http://www.palmetumapp.es/audiosMapa/";
		downloadSounds(baseUrl, language, 1, numberOfFilesToDownload);		
	}
	//En otro caso tenemos todo actualizado
	else{
		Ti.API.info('Los sonidos estan actualizados');
		Ti.App.fireEvent("descargaSonidosCompletada");
	}

}

function downloadSounds(baseUrl, language, fileNumber, numberOfFilesToDownload){
	var xhr = Titanium.Network.createHTTPClient({
	    onload: function() {
	        var f;
			if(Ti.Platform.name == "android"){
				f = Ti.Filesystem.getFile(Titanium.Filesystem.externalStorageDirectory, language + "/" + fileNumber + ".mp3");
			}
			else{
				f = Ti.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, language + "/" + fileNumber + ".mp3");
			}
	        
	        //Escribimos lo recibido en el fichero
	        f.write(this.responseData);
	        		
	       	//Si ya descargamos todos los sonidos mandamos el evento de finalizacion
			if(fileNumber == numberOfFilesToDownload-1){
				Ti.API.info('SONIDOS DESCARGADOS CORRECTAMENTE');
				Ti.App.fireEvent("descargaSonidosCompletada");
			}
			//Si no, hacemos recursividad para el siguiente fichero
			else{
				fileNumber = fileNumber + 1;
				downloadSounds(baseUrl, language, fileNumber, numberOfFilesToDownload);
			}
	    },
	    timeout: 10000
	});
	
	var urlSonido = baseUrl + language + "/" + fileNumber + ".mp3";
	Ti.API.info('urlSonido: ' + urlSonido);
	xhr.open('GET', urlSonido);
	xhr.send();
}

audioPlayer.addEventListener('complete', function() {
    Ti.App.fireEvent("SoundComplete");
});


module.exports = Sounds;